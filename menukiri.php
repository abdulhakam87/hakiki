<!-- left menu starts -->
<div class="sidebar-nav">
    <div class="nav-canvas">
        <ul class="nav nav-pills nav-stacked main-menu">
            <li class="nav-header">Main</li>
            <li>
                <a class="ajax-link" href="index.php?menu=1">
                    <i class="glyphicon glyphicon-home"></i><span> Dashboard</span>
                </a>
            </li>
            <li>
                <a class="ajax-link" href="index.php?menu=2">
                    <i class="glyphicon glyphicon-home"></i><span> Input Location</span>
                </a>
            </li>
        </ul>
        <label id="for-is-ajax" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>
    </div>
</div>
<!--/span-->
<!-- left menu ends -->
<noscript>
    <div class="alert alert-block col-md-12">
        <h4 class="alert-heading">Warning!</h4>
        <p>You need to have
            <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
            enabled to use this site.
        </p>
    </div>
</noscript>