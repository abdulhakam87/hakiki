<script>
    function initMap() {
        var pusat = {lat: -6.219359, lng: 107.168011};
        var map = new google.maps.Map(
        document.getElementById('map'), {zoom: 18, center: pusat});
        var a2 = {lat: -6.219392, lng: 107.167309};
        addMarker(pusat, map, 'A1');
        addMarker(a2, map, 'A2');
        var petaka1 = [
            {lat: -6.219170, lng: 107.168140},
            {lat: -6.219162, lng: 107.167735},
            {lat: -6.219602, lng: 107.167928},
            {lat: -6.219498, lng: 107.168448}
        ];
        var daeraha1 = new google.maps.Polygon({
            paths: petaka1,
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35
         });
        daeraha1.setMap(map);

        google.maps.event.addListener(daeraha1, "mouseover", function() {
            daeraha1.setOptions({fillColor: '#DE3030', fillOpacity: 0.5});
        });

        google.maps.event.addListener(daeraha1, "mouseout", function() {
            daeraha1.setOptions({fillColor: '#FF0000', fillOpacity: 0.35});
        });

        var petaka2 = [
            {lat: -6.219162, lng: 107.167735},
            {lat: -6.219321, lng: 107.166776},
            {lat: -6.219710, lng: 107.167176},
            {lat: -6.219602, lng: 107.167928}
        ];
        var daeraha2 = new google.maps.Polygon({
            paths: petaka2,
            strokeColor: '#66FF33',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#66FF33',
            fillOpacity: 0.35
         });
        daeraha2.setMap(map);

        google.maps.event.addListener(daeraha1, "mouseover", function() {
            daeraha2.setOptions({fillColor: '#33CC33', fillOpacity: 0.5});
        });

        google.maps.event.addListener(daeraha1, "mouseout", function() {
            daeraha2.setOptions({fillColor: '#66FF33', fillOpacity: 0.35});
        });
    };
    function addMarker(location, map, judul) {
        var image = new google.maps.MarkerImage('img/pin-battle.png',
          new google.maps.Size(35, 44),
          new google.maps.Point(0,0),
          new google.maps.Point(17, 44));
      var shape = {
          coord: [1, 1, 1, 20, 18, 20, 18 , 1],
          type: 'poly'
      };
        var marker = new google.maps.Marker({
            position: location,
            label: judul,
            icon: image,
            shape: shape,
            map: map
        });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCD-IY7FMQ9LJbbLAEKXf49AVRjYFoPUg&callback=initMap">
</script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Task', 'Hours per Day'],
            ['Work',     11],
            ['Eat',      2],
            ['Commute',  2],
            ['Watch TV', 2],
            ['Sleep',    7]
        ]);

        var options = {
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
    }
</script>
<script type="text/javascript">
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Work',     11],
          ['Eat',      2],
          ['Commute',  2],
          ['Watch TV', 2],
          ['Sleep',    7]
        ]);

        var options = {
          is3D: true
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
        chart.draw(data, options);
    }
</script>
<!-- content starts -->
<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Dashboard</a>
        </li>
    </ul>
</div>
<div class=" row">
    <div class="col-md-3 col-sm-3 col-xs-6">
        <a data-toggle="tooltip" title="503 Membayar Sesuai Target" class="well top-block" href="#">
            <i class="glyphicon glyphicon-user blue"></i>
            <div>Jumlah Kreditur</div>
            <div>507</div>
            <span class="notification">503</span>
        </a>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6">
        <a data-toggle="tooltip" title="Dari 4 Kreditur." class="well top-block" href="#">
            <i class="glyphicon glyphicon-usd green"></i>
            <div>Jumlah Tunggakan</div>
            <div>Rp. 228.000.345</div>
            <span class="notification green">4</span>
        </a>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6">
        <a data-toggle="tooltip" title="Tersedia 46 Petak" class="well top-block" href="#">
            <i class="glyphicon glyphicon-flag red"></i>
            <div>Jumlah Petak Tersedia</div>
            <div>46</div>
            <span></span>
        </a>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6">
        <a data-toggle="tooltip" title="12 new messages." class="well top-block" href="#">
            <i class="glyphicon glyphicon-envelope red"></i>
            <div>Messages</div>
            <div>25</div>
            <span class="notification red">12</span>
        </a>
    </div>
</div>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-globe"></i> Peta Kavling</h2>
                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default">
                        <i class="glyphicon glyphicon-cog"></i>
                    </a>
                    <a href="#" class="btn btn-minimize btn-round btn-default">
                        <i class="glyphicon glyphicon-chevron-up"></i>
                    </a>
                    <a href="#" class="btn btn-close btn-round btn-default">
                        <i class="glyphicon glyphicon-remove"></i>
                    </a>
                </div>
            </div>
            <div class="box-content">
                <div id="map" style="height: 500px; width: 100%"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="box col-md-6">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list-alt"></i> Pie</h2>
                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default">
                        <i class="glyphicon glyphicon-cog"></i>
                    </a>
                    <a href="#" class="btn btn-minimize btn-round btn-default">
                        <i class="glyphicon glyphicon-chevron-up"></i>
                    </a>
                    <a href="#" class="btn btn-close btn-round btn-default">
                        <i class="glyphicon glyphicon-remove"></i>
                    </a>
                </div>
            </div>
            <div class="box-content">
                <div id="piechart" style="height:300px"></div>
            </div>
        </div>
    </div>
    <div class="box col-md-6">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list-alt"></i> Donut</h2>
                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default">
                        <i class="glyphicon glyphicon-cog"></i>
                    </a>
                    <a href="#" class="btn btn-minimize btn-round btn-default">
                        <i class="glyphicon glyphicon-chevron-up"></i>
                    </a>
                    <a href="#" class="btn btn-close btn-round btn-default">
                        <i class="glyphicon glyphicon-remove"></i>
                    </a>
                </div>
            </div>
            <div class="box-content">
                <div id="donutchart" style="height: 300px;"></div>
            </div>
        </div>
    </div>
</div>
<!-- content ends -->