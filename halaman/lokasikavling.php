<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCD-IY7FMQ9LJbbLAEKXf49AVRjYFoPUg&callback=initMap">
</script>
<script>
  	// Note: This example requires that you consent to location sharing when
  	// prompted by your browser. If you see the error "The Geolocation service
  	// failed.", it means you probably did not give permission for the browser to
  	// locate you.
  	var map;
  	var petak = [
  		{lat: -6.240147, lng: 106.819947}, 
  		{lat: -6.240119, lng: 106.819829},
  		{lat: -6.240034, lng: 106.819855}, 
  		{lat: -6.240067, lng: 106.819969}
  	];
  	var tanda = [false, false, false, false];
  	var tengah = {lat: -6.240359, lng: 106.819618};
  	function initMap() {
    	map = new google.maps.Map(document.getElementById('map'), {
      		center: {lat: -6.240359, lng: 106.819618},
      		zoom: 20
    	});
  	}
	function myFunction(kode) {
  		if (navigator.geolocation) {
    		navigator.geolocation.getCurrentPosition(function(position) {
        		var pos = {
          			lat: position.coords.latitude,
          			lng: position.coords.longitude
        		};
        		var attrib = "";
        		if(kode == 9){
        			attrib = "center";
        			addMarker(pos, map);
        			tanda = [false, false, false, false];
        			map.setCenter(pos);
        		}else{
        			attrib = attrib + kode;
        			tanda[kode] = true;
        			// petak[kode] = pos;
        			if (tanda[0] && tanda[1] && tanda[2] && tanda[3]) {
        				gambarPetak(petak);
        			}
        		}
        		document.getElementById("longitude" + attrib).value = position.coords.longitude;
        		document.getElementById("latitude" + attrib).value = position.coords.latitude;
    		}, function() {
        		handleLocationError(true);
    		});
    	} else {
      		// Browser doesn't support Geolocation
      		handleLocationError(false);
    	}
	}
	function handleLocationError(browserHasGeolocation) {
    	alert(browserHasGeolocation ?
                          'Error: The Geolocation service failed.' :
                          'Error: Your browser doesn\'t support geolocation.');
  	}
  	function addMarker(location, map) {
        var marker = new google.maps.Marker({
            position: location,
            map: map
        });
    }
    function gambarPetak(denah) {
    	console.log("Disini");
        var daerah = new google.maps.Polygon({
            paths: denah,
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35
         });
        daerah.setMap(map);
    }
</script>


<div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Take Location</a>
        </li>
    </ul>
</div>
<div class="row">
    <div class="box col-md-12">
    	<div class="box-inner">
    		<div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-map-marker"></i> Form Pengisian Data Kavling</h2>
            </div>
    		<div class="box-content">
	         	<div class="row">
	    			<div class="box col-md-6">
	    				<div class="col-md-4">
	    					<H4 class="text-success">Longitude</H4>
	                	</div>
	                	<div class="col-md-4">
	                    	<H4 class="text-warning">Latitude</H4>
	                	</div>
	                	<div class="col-md-4">
	                		<p ontouchstart="myFunction(9)">Touch me!</p>
	                	</div>
	                	<hr style="width: 100%; color: black; height: 1px; background-color:black;">
	    				<div class="form-group has-success col-md-4">
	                    	<input type="text" class="form-control" id="longitudecenter" placeholder="Longitude">
	                	</div>
	                	<div class="form-group has-warning col-md-4">
	                    	<input type="text" class="form-control" id="latitudecenter" placeholder="Latitude">
	                	</div>
	                	<div class="form-group has-error col-md-4">
	                    	<button class="btn btn-danger" onclick="myFunction(9)" ontouchstart="myFunction(9)">Get center location</button>
	                	</div>
	                	<hr style="width: 100%; color: black; height: 1px; background-color:black;">
	                	<div class="form-group has-success col-md-4">
	                    	<input type="text" class="form-control" id="longitude0" placeholder="Longitude">
	                	</div>
	                	<div class="form-group has-warning col-md-4">
	                    	<input type="text" class="form-control" id="latitude0" placeholder="Latitude">
	                	</div>
	                	<div class="form-group has-error col-md-4">
	                    	<button class="btn btn-info" onclick="myFunction(0)">Get Right Top</button>
	                	</div>
	                	<div class="form-group has-success col-md-4">
	                    	<input type="text" class="form-control" id="longitude1" placeholder="Longitude">
	                	</div>
	                	<div class="form-group has-warning col-md-4">
	                    	<input type="text" class="form-control" id="latitude1" placeholder="Latitude">
	                	</div>
	                	<div class="form-group has-error col-md-4">
	                    	<button class="btn btn-info" onclick="myFunction(1)">Get Left Top</button>
	                	</div>
	                	<div class="form-group has-success col-md-4">
	                    	<input type="text" class="form-control" id="longitude2" placeholder="Longitude">
	                	</div>
	                	<div class="form-group has-warning col-md-4">
	                    	<input type="text" class="form-control" id="latitude2" placeholder="Latitude">
	                	</div>
	                	<div class="form-group has-error col-md-4">
	                    	<button class="btn btn-info" onclick="myFunction(2)">Get Left Bottom</button>
	                	</div>
	                	<div class="form-group has-success col-md-4">
	                    	<input type="text" class="form-control" id="longitude3" placeholder="Longitude">
	                	</div>
	                	<div class="form-group has-warning col-md-4">
	                    	<input type="text" class="form-control" id="latitude3" placeholder="Latitude">
	                	</div>
	                	<div class="form-group has-error col-md-4">
	                    	<button class="btn btn-info" onclick="myFunction(3)">Get Right Bottom</button>
	                	</div>
	    			</div>
	    			<div class="box col-md-6">
	    				<div id="map" style="height: 350px; width: 100%"></div>
	    			</div>
	    			<hr style="width: 100%; color: black; height: 1px; background-color:black;">
	    			<div class="box col-md-12">
	    				<div id="map" style="height: 350px; width: 100%"></div>
	    			</div>
	    		</div>
	    	</div>
    	</div>
    </div>
</div>